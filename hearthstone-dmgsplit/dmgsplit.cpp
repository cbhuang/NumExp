/*
 * Numerically simulate lethal rate of Amaz Hearthstone problem https://youtu.be/clEKDpjQ7Ok
 * 
 * NOTE: only one specific lethal pattern is tested
 * 
 */

#include <iostream>  // cout
#include <cstdlib>  // rand()

// pick a random non-zero indice from an array
int picktarget(int n_targets, const int *targets) {

    // initialize a non-zero vector for recording
    int n_nonzero = 0;
    int *nonzero_targets = new int[n_targets];

    for(int i = 0; i < n_targets; i++){
        if (targets[i] > 0) {
            nonzero_targets[n_nonzero] = i;  // record indice of nonzero entry
            n_nonzero++;  // count++
        }
    }

    // return a random indice with non-zero entry
    return nonzero_targets[rand() % n_nonzero];
}


int main(int argc, char** argv){

    // parameters
    const int n_shot = 8;
    const int n_targets = 4;
    const int *targets = new int[4]{4, 4, 6, 8};
    const int iter = 100000000;  // # of experiments

    // temp/common variables
    int *targets_left = new int[n_targets];  // target instance to shoot
    int picked;  // temp variable for picked target
    int n, i;  // loop variables
    int n_stegodon_killed = 0;
    int n_lethal = 0;

    // init
    srand(time(NULL));

    // main loop
    for (n = 0; n < iter; n++) {

        // initialize a target instance
        for (i = 0; i < n_targets; i++)
    	    targets_left[i] = targets[i];

        // shoot
        for (i = 0; i < n_shot; i++) {
       	    // pick
            picked = picktarget(n_targets, targets_left);
            // hit
            targets_left[picked] -= 1;
        }

        // count
        if (targets_left[2] == 0) {
        	n_stegodon_killed++;
        	if (targets_left[3] < targets[3]) {
            	n_lethal++;
        	}
        }

        // print result
        /*for(int i=0; i < n_targets; i++)
             cout << targets_left[i];
        cout << "\n";
        */
    }

    std::cout << "rate killing stegodon: " << (1.0 * n_stegodon_killed / iter)
    	 << "\nrate lethal: " << (1.0 * n_lethal / iter);

    return 0;
}
