################################
    Two-Level System Summary
################################

* Atomic units were used:

  =================   ========================
  Dimension           Value
  =================   ========================
  Time                24.19 as
  Energy              27.21 eV = 1 Hartree
  Angular Frequency   (2*pi)/(Time unit) = 2.60e17 (rad/time unit)
  Length              52.92 pm = 1 Bohr
  =================   ========================

No EM-Field Coupling
==========================

* 無能障dA=0：阿就自然頻率來回....

* 有能障：低能區機率恆不歸零、高能區機率恆不為1

    e.g. dA=\| B \|=0.1，時低能區機率在0.2~1之間來回。

    .. image:: B_em=0/dA=-B_rho00.png
       :height: 300

Medium Frequency
==========================

Parameters: ``A=-0.2, B=-0.1, w=0.1``

* 數學形式上，B_em 跟 B 都是off-diagonal term，會互相疊加，
  B_em >> B 時，角頻率變成由 B_em 主導（變5倍）。
  反之就還是由原來的 B 主導，B_em變成perturbation

* B_em >> B 時，在某些相位之時，好像兩個成份「相消」了一樣，頻率出現放慢現象

* B_em << B 時，只有頻率變化


High Frequency
==========================

Parameters: ``A=-0.2, w=0.5``

無能障
------

* Strong coupling (B_em=5B) 時，傳播速度被 w_em所主導，變成5倍速。
  隨相位變化，同樣觀察到傳輸妨礙

* weak coupling時，根本抓癢，而且 B_em = 0.5B 還是算"weak"


有能障(dA=B)
--------------
* **Weak coupling**：一樣抓癢
* **strong coupling**： *「低能量區之機率下限」被外場整個打穿！*

.. image:: w=0.5(high-freq)/dA=B,B_em=5B_rho00.png
   :height: 300
