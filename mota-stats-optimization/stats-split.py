"""
Compute the optimal distribution of stats
"""

from dataclasses import dataclass
from abc import abstractmethod
from copy import deepcopy


@dataclass
class Character:
    hp: int
    atk: int
    def_: int

    @abstractmethod
    def attack(self, defender: 'Character') -> int:
        """Specs of the attack function

        Args:
            defender: hero or monster

        Returns:
            int: damage dealt
        """
        pass

    def is_dead(self):
        return self.hp <= 0

@dataclass
class Monster(Character):
    atk_first: bool = False
    atk_magic: bool = False

    def attack(self, defender: 'Hero') -> int:

        if self.atk_magic:
            dmg = max(0, self.atk - defender.mdf)
        else:
            dmg = max(0, self.atk - defender.def_)

        defender.hp -= dmg
        return dmg

@dataclass
class Hero(Character):
    mdf: int

    def attack(self, defender: 'Monster') -> int:
        dmg = max(0, self.atk - defender.def_)
        defender.hp -= dmg
        return dmg


def turn(hero: Hero, monster: Monster) -> int:
    """Takes a turn

    Args:
        hero:
        monster:

    Returns:
        int: status code - 0=continue, 1=win, 2=lose, 3=infinite_loop
    """

    # Case 1: monster attacks first
    if monster.atk_first:
        dmg_hero = monster.attack(hero)
        if hero.is_dead():
           return 2

        dmg_monster = hero.attack(monster)
        if monster.is_dead():
            return 1

    # Case 2: hero attacks first
    else:
        dmg_monster = hero.attack(monster)
        if monster.is_dead():
            return 1

        dmg_hero = monster.attack(hero)
        if hero.is_dead():
            return 2

    # Common ending logic: prevent infinite loop
    if 0 == dmg_hero == dmg_monster:
        return 3
    else:
        return 0


def fight(hero: Hero, monster: Monster) -> int:
    """Fight till the end

    Args:
        hero:
        monster:

    Returns:
        int: hero damage received
    """
    hero_hp_begin = deepcopy(hero.hp)
    round = 0
    status = 0
    while status == 0:
        round += 1
        status = turn(hero, monster)

    # Hero wins
    if status == 1:
        #print(f"Hero wins! damage: {hero_hp_begin - hero.hp}")
        return hero_hp_begin - hero.hp
    # Hero fails
    elif status == 2:
        return hero_hp_begin
    # If both parties couldn't attack, it means the hero loses
    elif status == 3:
        return hero_hp_begin
    else:
        raise NotImplementedError(f"unknown status code: {status}")


if __name__ == "__main__":

    # ===========================
    # Scan best damage
    # ===========================

    # ========== Params ==========
    # hero
    hero_hp = 50000
    hero_mdf = 770  # irrelevant
    a_plus_d = 1065 + 772
    step = 3
    atk_min = 1001
    def_min = 380
    atk_max = a_plus_d - def_min

    # monster
    monster_hp = 2000
    monster_atk = 1300
    monster_def = 800
    # ========== End params ==========

    # Brute-force iteration by increasing atk
    atk_best_min = 0
    atk_best_max = 0
    dmg_best = hero_hp
    for atk in range(atk_min, atk_max + 1, step):
        def_ = a_plus_d - atk
        hero = Hero(hp=hero_hp, atk=atk, def_=def_, mdf=hero_mdf)
        monster = Monster(hp=monster_hp, atk=monster_atk, def_=monster_def)
        dmg = fight(hero, monster)
        # If dmg improves, update best value and range
        if dmg < dmg_best:
            dmg_best = dmg
            atk_best_min = atk
            atk_best_max = atk
        # If dmg is the same as the best, update the range
        elif dmg == dmg_best:
            atk_best_max = atk
        else:
            pass  # ignore suboptimal results
        if atk % 10 == 0:
            print(f"{atk=}, {def_=} {dmg=}, {dmg_best=}")

    print(f"Best HP cost: {dmg_best}")
    print(f"Best atk range: {atk_best_min} - {atk_best_max}")
    print(f"Best def range: {a_plus_d - atk_best_min} - {a_plus_d - atk_best_max}")
