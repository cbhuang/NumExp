/*
 *  check the distribution of a random splitting of a segment
 *      CDF = (1-x)^k
 *      PDF = k(1-x)^(k-1)
 *  where k = #breakpoints
 */

package main

import (
	"fmt"
	"math"
	"math/rand"
	"sort"
	"time"
)

// CDF to be checked
func cdf(x float64, k int) float64 {
	return 1 - math.Pow(1-x, float64(k))
}

func main() {

	// parameters
	n_bp := 5         // # of breakpoints
	n_exp := 10000000 // # of repeated experiments
	l_min := 0.90     // cdf of segment length range (l_min, l_max)
	l_max := 1.0

	// initialize
	gen := rand.New(rand.NewSource(time.Now().UnixNano())) // random number generator

	// common variables
	breakpts := make([]float64, n_bp+2)
	breakpts[0] = 0.0
	breakpts[n_bp+1] = 1.0

	lengths := make([]float64, n_bp+1)
	tmp_len := float64(0.0)

	// count
	cnt := 0                                                              // positive counts
	cdf_expect := float64(n_bp+1) * (cdf(l_max, n_bp) - cdf(l_min, n_bp)) // expected cdf value, normalized to # of segments

	// experiment
	for n := 0; n < n_exp; n++ {

		// generate breakpoints
		for i := 1; i <= n_bp; i++ {
			breakpts[i] = gen.Float64()
		}
		sort.Float64s(breakpts)

		// calculate length
		for i := 0; i <= n_bp; i++ {
			tmp_len = breakpts[i+1] - breakpts[i]
			lengths[i] = tmp_len

			// count
			if l_min < tmp_len && tmp_len < l_max {
				cnt++
			}
		}
	}

	// show
	fmt.Printf("======= %d experiments was performed ======\n", n_exp)
	fmt.Printf("expected cdf value: %.13f \n", cdf_expect)
	fmt.Printf("experimental cdf value: %.13f \n", float64(cnt)/float64(n_exp))
}
